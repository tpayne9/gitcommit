*! 0.1 TCP 24jul2020
// report git commit hash
cap prog drop gitcommit
program gitcommit

version 16.1

syntax [anything]

if "`anything'" == "" local git_dir = "./"
else local git_dir = "`anything'"
cap confirm file `git_dir'/.git/logs/HEAD
if !_rc {
	tempname fh
	file open `fh' using `"`git_dir'.git/logs/HEAD"', read
	file read `fh' line
	local linenum = 0 
	while r(eof) == 0 {
		local ++linenum
		if regexm("`line'" , "(.*clone: from )(.*)") {
			local cloneurl = regexs(2)  
		}
		
		local commit_SHA : word 2 of `line'
		file read `fh' line
	}
}

di "`commit_SHA'"
di "`cloneurl'"


end


//eof
